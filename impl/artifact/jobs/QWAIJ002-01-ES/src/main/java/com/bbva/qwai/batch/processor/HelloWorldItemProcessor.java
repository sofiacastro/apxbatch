package com.bbva.qwai.batch.processor;

import org.springframework.batch.item.ItemProcessor;

public class HelloWorldItemProcessor implements ItemProcessor<String,String> {

	@Override
	public String process(String arg0) throws Exception {
		// TODO Auto-generated method stub
		//arg0 es hello|world
		// lo transformamos a hello world
		
		String[] split = arg0.split(",");
		return split[0] + " " + split[1];
	}

}
